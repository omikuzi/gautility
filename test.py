# -*- coding: utf-8 -*-
"""
GAのテストを行うスクリプトです
"""
import random
import GAUtility
def next_generation(population, selection_type, crossover_type, mutation_rate, **args):
    """
    集団を次世代へ移行します
    """
    pop_size = len(population)
    ng_pop = []
    for i in range(int(pop_size/2)):
        parents = GAUtility.selection(population, selection_type, **args)
        children = GAUtility.crossover(parents, crossover_type, **args)
        for child in children:
            new_child = GAUtility.mutation(child, mutation_rate, **args)
            ng_pop.append(new_child)
    if len(ng_pop) < pop_size:
        parents = GAUtility.selection(population, selection_type, **args)
        children = GAUtility.crossover(parents, crossover_type, **args)
        child = GAUtility.mutation(children[0], mutation_rate, **args)
        ng_pop.append(child)
    return ng_pop[:pop_size]
def test():
    """
    module test func
    """
    test_population = [random_individual(genom_size=50, fitness=x+1) for x in range(100)]
    args = {
        'tournament_size':2500,
        'reverse':False,
        'genom_size':16,
        'elete_size':1,
        'point':2,
        'genom_type':GAUtility.GENOM_TYPE['LIST']
    }
    print(test_population)
    selection = {
        'ROULETTE':0,
        'RANK':1,
        'TOURNAMENT':2
    }
    crossover = {
        'CX':0,
        'OX':1,
        'PMX':2,
    }
    for selection_name, selection_type in selection.items():
        for crossover_name, crossover_type in crossover.items():
            print(selection_name, crossover_name)
            ng_pop = next_generation(   test_population,
                                        selection_type,
                                        crossover_type,
                                        100,
                                        **args)
            print(ng_pop)
    args['genom_type'] = GAUtility.GENOM_TYPE['BIT']
    crossover = {
        'UNIFORM':3,
        'MULTI_POINT':4
    }
    genom_min  = -(2**(args['genom_size']-1))
    genom_max  = 2**(args['genom_size']-1)-1
    test_population = [[random.randrange(genom_min,genom_max),x+1] for x in range(100)]
    for selection_name, selection_type in selection.items():
        for crossover_name, crossover_type in crossover.items():
            print(selection_name, crossover_name)
            ng_pop = next_generation(   test_population,
                                        selection_type,
                                        crossover_type,
                                        100,
                                        **args)
            print(ng_pop)
def random_individual(genom_size=10, fitness=0):
    """
    ランダムな個体を作成し返す関数です
    """
    genom = [x for x in range(genom_size)]
    random.shuffle(genom)
    return (tuple(genom), fitness)
if __name__ == '__main__':
    test()
