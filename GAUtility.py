# -*- coding:utf-8 -*-
"""
遺伝的アルゴリズムを利用するための各種機能を提供します

GAUtility
|
|-selection
| |-roulette_selection
| |-rank_selection
| |-tournament_selection
| |-elite_selection
|
|-crossover
| |-cycle_crossover
| |-order_crossover
| |-partially_mapped_crossover
| |-multipoint_crossover
| |-uniform_crossover
|
|-mutation
| |-bit_mutation
| |-list_mutation

このモジュールでは遺伝的アルゴリズム（GA）において利用される手法の実装を提供します。
このモジュールでは選択、交差、突然変異における実装を提供しますが、
GA全体の制御に関する機能は提供されません。
またこのモジュール中で利用することができる個体の構造は以下のとおりです
順列方：tuple(list,int)
BIT型：tuple(int,int)
それぞれタプル内の前者が遺伝子で後者が適応度である
これらの個体どちらかの種別をlistにまとめたものを集団とし各関数で扱います

以下に利用例のサンプルを示します

def next_generation(population, selection_type, crossover_type, mutation_rate, **args):
    \"\"\"
    集団を次世代へ移行します
    \"\"\"
    #集団の大きさを取得
    pop_size = len(population)
    #新世代
    ng_pop = []
    for i in range(int(pop_size/2)):
        #指定された選択を実行、親となる個体2体を取得
        parents = GAUtility.selection(population, selection_type, **args)
        #選択された個体を交叉、新世代２体を取得
        children = GAUtility.crossover(parents, crossover_type, **args)
        #それぞれ新世代へ登録
        for child in children:
            #新世代の個体を突然変異する
            new_child = GAUtility.mutation(child, mutation_rate, **args)
            #新世代へ登録
            ng_pop.append(new_child)
    #集団が奇数の場合個体数が足りないためもう一体個体を追加する
    if len(ng_pop) < pop_size:
        parents = GAUtility.selection(population, selection_type, **args)
        children = GAUtility.crossover(parents, crossover_type, **args)
        child = GAUtility.mutation(children[0], mutation_rate, **args)
        ng_pop.append(child)
    #作成された新世代を返す
    return ng_pop[:pop_size]
"""
import random
import gene_controller.individual_cross as ic

__author__  = "J409<path@omikuzi.com>"
__version__ = "1.0.0"
__date__    = "01 March 2016"
#選択手法を指定するためのint定数です
SELECTION = {
    'ROULETTE': 0,
    'RANK': 1,
    'TOURNAMENT': 2
}
#交叉手法を指定するためのint定数です
CROSSOVER = {
    'CX': 0,
    'OX': 1,
    'PMX': 2,
    'UNIFORM': 3,
    'MULTI_POINT': 4
}
#遺伝子の形式を指定するためのint定数です
GENOM_TYPE = {
    'LIST': 0,
    'BIT': 1
}
def selection(population, selection_type, **args):
    """集団と選択種別を受け取り、指定された選択を行い個体を返す関数です
    引数
    population -- 個体の集合を表すListです
    selection_type -- 選択の指定を表すintです
                      クラス変数SELECTIONの値に基づきます。
    args -- 選択で用いられる値を格納したdictです。
            以下の値を利用する可能性があります。
            revers -- 最小化もしくは最大化を切り替えるboolです
                    　Trueならば最小化となります
            tournament_size -- トーナメント選択に用いるintです
                            　　トーナメントサイズを指定します
    戻り値:list
    選択された個体二体で構成されたlistです
    """
    population = list(population[:])
    if 'reverse' in args.keys():
        args['reverse'] = False
    population.sort(key=lambda x:x[1], reverse=not args['reverse'])
    parents = []
    if selection_type == SELECTION['ROULETTE']:
        parents.append(roulette_selection(population))
        population.remove(parents[0])
        parents.append(roulette_selection(population))
    elif selection_type == SELECTION['RANK']:
        parents.append(rank_selection(population))
        population.remove(parents[0])
        parents.append(rank_selection(population))
    elif selection_type == SELECTION['TOURNAMENT']:
        tournament_size = args['tournament_size']
        if tournament_size < 1:
            tournament_size = 1
        elif tournament_size > len(population):
            tournament_size = len(population)
        new_parent = tournament_selection(  population,
                                            tournament_size,
                                            reverse=args['reverse'])
        parents.append(new_parent)
        population.remove(parents[0])
        if tournament_size > len(population):
            tournament_size = len(population)
        new_parent = tournament_selection(  population,
                                            tournament_size,
                                            reverse=args['reverse'])
        parents.append(new_parent)
    return parents
def roulette_selection(population):
    """ルーレット選択を行う関数です
    引数
    population -- 個体の集合を表すListです
                　ソート済みである必要があります
    戻り値:list
    選択された個体二体で構成されたlistです
    """
    total_fitness = 0
    for individual in population:
        if individual[1] >= 0:
            total_fitness += individual[1]
        else:
            raise Exception()
    target = random.randrange(total_fitness)
    for individual in population:
        target -= individual[1]
        if target < 0:
            return individual
def rank_selection(population):
    """ランク選択を行う関数です
    引数
    population -- 個体の集合を表すListです
                　ソート済みである必要があります
    戻り値:list
    選択された個体二体で構成されたlistです
    """
    total_rank = sum(range(len(population)))
    target = random.randrange(total_rank)
    for index, individual in enumerate(population):
        target -= (len(population)-index)
        if target < 0:
            return individual
def tournament_selection(population, size, reverse=False):
    """トーナメント選択を行う関数です
    引数
    population -- 個体の集合を表すListです
    size -- トーナメントサイズを表すintです
    戻り値:list
    選択された個体二体で構成されたlistです
    """
    if not reverse:
        return max(random.sample(population, size), key=lambda x:x[1])
    else:
        return min(random.sample(population, size), key=lambda x:x[1])
def elite_selection(population, size):
    """
    エリート選択を行う関数です
    population -- 固体の集合を表すListです
    size -- トーナメントサイズを示すintです
    戻り値:list
    選択された個体で構成されたlistです
    """
    return population[:size]
def crossover(parents, crossover_type, **args):
    """
    親と交叉種別を受け取って交叉を行う関数です
    parents -- 個体二つを含むListです
    crossover_type -- 交叉種別を指定するintです
    args -- 選択で用いられる値を格納したdictです。
            以下の値を利用する可能性があります。
            point -- 多点交叉を行うときに交差点の個数として利用さるint型です
            genom_size -- BIT列に対して交叉を行うときに利用される
                        　BIT長を示すint型です
    戻り値:list
    交差結果として得られた個体二体で構成されるlistです
    """
    children = []
    if crossover_type == CROSSOVER['CX']:
        children = cx_crossover(parents)
    elif crossover_type == CROSSOVER['OX']:
        children = ox_crossover(parents)
    elif crossover_type == CROSSOVER['PMX']:
        children = pmx_crossover(parents)
    elif crossover_type == CROSSOVER['MULTI_POINT']:
        children = multipoint_crossover(parents,args['genom_size'],args['point'])
    elif crossover_type == CROSSOVER['UNIFORM']:
        children = uniform_crossover(parents,args['genom_size'])
    return children
def cx_crossover(parents):
    """
    cycle crossoverを行う関数です
    parents -- 交叉に用いる二体の親個体です
    戻り値:list
    交差結果として得られた個体二体で構成されるlistです
    """
    size = len(parents[0][0])
    parents = [parent[0] for parent in parents]
    children = []
    children.append([None for x in range(size)])
    children.append([None for x in range(size)])
    carrent_gene = 0
    cycled = [0]
    start = 0
    children[0][0] = parents[0][0]
    children[1][0] = parents[1][0]
    while True:
        carrent_gene = parents[0].index(parents[1][carrent_gene])
        cycled.append(carrent_gene)
        children[0][carrent_gene] = parents[0][carrent_gene]
        children[1][carrent_gene] = parents[1][carrent_gene]
        if carrent_gene == start:
            if children[0].count(None) > 0:
                children.reverse()
                for index in range(size):
                    if index not in cycled:
                        carrent_gene = index
                        start = index
                        cycled.append(index)
                        break
            else:
                break
    return [(tuple(children[0]), 0), (tuple(children[1]), 0)]
cycle_crossover = cx_crossover
def ox_crossover(parents):
    """
    order crossoverを行う関数です
    parents -- 交叉に用いる二体の親個体です
    戻り値:list
    交差結果として得られた個体二体で構成されるlistです
    """
    #親の分解
    parents = [parent[0] for parent in parents]
    #順列のサイズ
    size = len(parents[0])
    #要素の選択
    select = random.sample(range(size), 2)
    select.sort()
    #シーケンスの作成
    sequences = []
    sequences.append(list(parents[0][select[1]:]))
    sequences[0].extend(parents[0][:select[1]])
    sequences.append(list(parents[1][select[1]:]))
    sequences[1].extend(parents[1][:select[1]])
    #選択された部分のみを持つ子を作成
    children = []
    children.append(list(parents[0][:]))
    children[0][select[1]:] = [None for i in children[0][select[1]:]]
    children[0][:select[0]] = [None for i in children[0][:select[0]]]
    children.append(list(parents[1][:]))
    children[1][select[1]:] = [None for i in children[1][select[1]:]]
    children[1][:select[0]] = [None for i in children[1][:select[0]]]
    #シーケンスより固定されている要素の削除
    #sequences[0]からchildren[1]に含まれる要素を削除
    sequences[0] = [x for x in sequences[0] if x not in children[1]]
    #sequences[1]からchildren[0]に含まれる要素を削除
    sequences[1] = [x for x in sequences[1] if x not in children[0]]
    #子にシーケンスを挿入
    children[0][select[1]:] = [sequences[1][i] for i in range(len(children[0][select[1]:]))]
    children[0][:select[0]] = sequences[1][len(children[0][select[1]:]):]
    children[1][select[1]:] = [sequences[0][i] for i in range(len(children[1][select[1]:]))]
    children[1][:select[0]] = sequences[0][len(children[1][select[1]:]):]
    return [[tuple(children[0]), 0], [tuple(children[1]), 0]]
order_crossover = ox_crossover
def pmx_crossover(parents):
    """
    Partially-mapped crossoverを行う関数です
    parents -- 交叉に用いる二体の親個体を含むlistです
    戻り値:list
    交差結果として得られた個体二体で構成されるlistです
    """
    #親の分解
    parents = [parent[0] for parent in parents]
    #順列のサイズ
    size = len(parents[0])
    #要素の選択
    select = random.sample(range(size), 2)
    select.sort()
    #子の作成
    children = []
    children.append(list(parents[0][:]))
    children.append(list(parents[1][:]))
    children[0][select[0]:select[1]] = parents[1][select[0]:select[1]]
    children[1][select[0]:select[1]] = parents[0][select[0]:select[1]]
    change_map = []
    change_map.append(list(parents[0][select[0]:select[1]]))
    change_map.append(list(parents[1][select[0]:select[1]]))
    for i in range(len(change_map[0])):
    	if change_map[0][i] == change_map[1][i]:
    		change_map[0][i] = change_map[1][i] = None
    while None in change_map[0]:
    	change_map[0].remove(None)
    	change_map[1].remove(None)
    for s_2 in change_map[1][:]:
    	if s_2 in change_map[0]:
    		i = change_map[0].index(s_2)
    		change_map[1][change_map[1].index(s_2)] = change_map[1][i]
    		change_map[0][i] = change_map[1][i] = None
    for i in range(len(change_map[0])):
    	if change_map[0][i] == change_map[1][i]:
    		change_map[0][i] = change_map[1][i] = None
    while None in change_map[0]:
    	change_map[0].remove(None)
    	change_map[1].remove(None)
    range_list = [x for x in range(select[0], select[1])]
    __pmx_list_convert(children[0], change_map[0], change_map[1], range_list)
    __pmx_list_convert(children[1], change_map[0], change_map[1], range_list)
    return [[tuple(children[0]), 0], [tuple(children[1]), 0]]
def __pmx_list_convert(target_list, change_map1, change_map2, conved=None):
    """
    PMXに用いる関数
    """
    conved = conved[:] if conved is not None else []
    for source in change_map1:
        if source in target_list:
            i = target_list.index(source)
            if i in conved:
                if source in target_list[i+1:]:
                    i = i + target_list[i+1:].index(source) + 1
            if i not in conved:
                target_list[i] = change_map2[change_map1.index(source)]
                conved.append(i)
    for source in change_map2:
        if source in target_list:
            i = target_list.index(source)
            if i in conved:
                if source in target_list[i+1:]:
                    i = i + target_list[i+1:].index(source) + 1
            if i not in conved:
                target_list[i] = change_map1[change_map2.index(source)]
partially_mapped_crossover = pmx_crossover
def multipoint_crossover(parents, genom_size, point=2):
    """
    多点交差を行う関数です
    parents -- 交叉に用いる二体の親個体を含むlistです
    genom_size -- 遺伝子の長さを示すintです
    point -- 交差点の個数を示すintです
    戻り値:list
    交差結果として得られた個体二体で構成されるlistです
    """
    new_genom = ic.multipoint_crossover(point, parents[0][0], parents[1][0], genom_size)
    return [[genom,0] for genom in new_genom]
def uniform_crossover(parents, genom_size):
    """
    一様交差を行う関数です
    parents -- 交叉に用いる二体の親個体を含むlistです
    genom_size -- 遺伝子の長さを示すintです
    戻り値:list
    交差結果として得られた個体二体で構成されるlistです
    """
    new_genom = ic.uniform_crossover(parents[0][0], parents[1][0], genom_size)
    return [[genom,0] for genom in new_genom]
def mutation(individual, rate, genom_type, **args):
    """
    個体の突然変異を行う関数です
    individual -- 突然変異を行う個体です
    rate -- 遺伝子の変異確率を示すintです
    genom_type -- 遺伝子の種別を示すintです
    args -- 突然変異で利用される値を格納したdictです
            以下の値を利用します
            genom_size -- 遺伝子の長さを示すintです
    戻り値:individual
    突然変異を行った後の個体です
    """
    if genom_type == GENOM_TYPE['LIST']:
        new_individual = list_mutation(individual, rate)
    elif genom_type == GENOM_TYPE['BIT']:
        new_individual = bit_mutation(individual, rate ,args['genom_size'])
    return new_individual
def list_mutation(individual, rate):
    """
    リストを遺伝子に持つ個体の突然変異を行う関数です
    individual -- 突然変異を行う個体です
    rate -- 遺伝子の変異確率を示すintです
    戻り値:individual
    突然変異を行った後の個体です
    """
    if rate == 0:
        return [individual[0][:], 0]
    genom = list(individual[0])
    for index, gene in enumerate(genom):
        if random.randrange(rate) == 0:
            old_genom = genom[:]
            old_genom.remove(gene)
            target_index = genom.index(random.sample(old_genom, 1)[0])
            genom[index], genom[target_index] = genom[target_index], genom[index]
    return [tuple(genom), 0]
def bit_mutation(individual, rate, genom_size):
    """
    ビット列を遺伝子に持つ個体の突然変異を行う関数です
    individual -- 突然変異を行う個体です
    rate -- 遺伝子の変異確率を示すintです
    戻り値:individual
    突然変異を行った後の個体です
    """
    if rate == 0:
        return [individual[0], 0]
    genom = individual[0]
    ic.gene_mutation(genom, genom_size, rate)
    return [genom, 0]
