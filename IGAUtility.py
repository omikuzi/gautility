# -*- coding: utf-8 -*-
"""
IronPythonを利用し主にUnityで利用することを前提としたGAUtilityの実装です
IronPythonを導入し、IronPythonの標準ライブラリを利用できる状態にあるUnity上で動作します
3.4系のGAUtilityとの差異は以下のとおりです
・BIT型遺伝子はサポートが存在しない
・メソッドを全てクラスGAUtilityにクラスメソッドとして定義
・個体は全てタプルで定義される、つまり(tuple,int)の形でそれぞれ順列、適応度をさす
・集団も同様に個体を要素として持つタプルとして定義される
・クラスGAUtility内に次世代への移行を行うメソッドが実装されている
"""
import clr
clr.AddReferenceByPartialName('UnityEngine')
import UnityEngine
import sys
sys.path.append(UnityEngine.Application.dataPath + '/../PythonScripts/Lib')
import random

__author__  = "J409<path@omikuzi.com>"
__version__ = "1.0.0"
__date__    = "01 March 2016"
class GAUtility:
    """
    遺伝的アルゴリズムを利用するための各種機能を提供します

    GAUtility
    |
    |-next_generation
    |
    |-selection
    | |-roulette_selection
    | |-rank_selection
    | |-tournament_selection
    | |-elite_selection
    |
    |-crossover
    | |-cycle_crossover
    | |-order_crossover
    | |-partially_mapped_crossover
    |
    |-mutation

    このモジュールでは遺伝的アルゴリズム（GA）において利用される手法の実装を提供します。
    このモジュールでは選択、交差、突然変異における実装を提供しますが、
    GA全体の制御に関する機能は提供されません。
    またこのモジュール中で利用することができる個体の構造は以下のとおりです
    """
    SELECTION = {
        'ROULETTE':0,
        'RANK':1,
        'TOURNAMENT':2
    }
    CROSSOVER = {
        'CX':0,
        'OX':1,
        'PMX':2
    }
    @classmethod
    def next_generation(cls, old_pop, selection_type, crossover_type, mutation_rate, **args):
        """
        集団を次世代へ移行する関数です
        old_pop -- 親世代の集団です
        selection_type　-- 選択手法を選択する際に利用する値です
                           定数SELECTIONの値を利用します
        crossover_type　-- 交叉手法を選択する際に利用する値です
                          定数CROSSOVERの値を利用します
        mutation_rate -- 突然変異の確立を指定するint型です、
                         1/mutation_rateの確立で変異を行います
        args -- 各選択、交叉において利用されるdictです
                各関数にそのまま渡されます
        返り値：集団
        新しく作成された個体を含む、親世代と同じサイズの集団です
        """
        pop_size = len(old_pop)
        ng_pop = []
        for i in xrange(int(pop_size/2)):
            parents = cls.selection(old_pop, selection_type, **args)
            children = cls.crossover(parents, crossover_type)
            for child in children:
                new_child = cls.mutation(child, mutation_rate)
                ng_pop.append(new_child)
        if len(ng_pop) < pop_size:
            parents = cls.selection(old_pop, selection_type, **args)
            children = cls.crossover(parents, crossover_type)
            child = cls.mutation(children[0], mutation_rate)
            ng_pop.append(child)
        return tuple(ng_pop[:pop_size])
    @classmethod
    def selection(cls, population, selection_type, **args):
        """集団と選択種別を受け取り、指定された選択を行い個体を返す関数です
        引数
        population -- 個体の集合を表すtupleです
        selection_type -- 選択の指定を表すintです
                          クラス変数SELECTIONの値に基づきます。
        args -- 選択で用いられる値を格納したdictです。
                以下の値を利用する可能性があります。
                revers -- 最小化もしくは最大化を切り替えるboolです
                        　Trueならば最小化となります
                tournament_size -- トーナメント選択に用いるintです
                                　　トーナメントサイズを指定します
        戻り値:tuple
        選択された個体二体で構成されたtupleです
        """
        population = list(population[:])
        population.sort(key=lambda x: x[1], reverse=not args['reverse'])
        parents = []
        if selection_type == cls.SELECTION['ROULETTE']:
            parents.append(cls.roulette_selection(population))
            population.remove(parents[0])
            parents.append(cls.roulette_selection(population))
        elif selection_type == cls.SELECTION['RANK']:
            parents.append(cls.rank_selection(population))
            population.remove(parents[0])
            parents.append(cls.rank_selection(population))
        elif selection_type == cls.SELECTION['TOURNAMENT']:
            tournament_size = args['tournament_size']
            if tournament_size < 1:
                tournament_size = 1
            elif tournament_size > len(population):
                tournament_size = len(population)
            parents.append(cls.tournament_selection(population, tournament_size,reverse = args['reverse']))
            population.remove(parents[0])
            if tournament_size > len(population):
                tournament_size = len(population)
            parents.append(cls.tournament_selection(population, tournament_size,reverse = args['reverse']))
        return parents
    @classmethod
    def roulette_selection(cls, population):
        """ルーレット選択を行う関数です
        引数
        population -- 個体の集合を表すListです
                    　ソート済みである必要があります
        戻り値:tuple
        選択された個体二体で構成されたtupleです
        """
        total_fitness = 0
        for individual in population:
            if individual[1] >= 0:
                total_fitness += individual[1]
            else:
                raise Exception()
        target = random.randrange(total_fitness)
        for individual in population:
            target -= individual[1]
            if target < 0:
                return individual
    @classmethod
    def rank_selection(cls, population):
        """ランク選択を行う関数です
        引数
        population -- 個体の集合を表すListです
                    　ソート済みである必要があります
        戻り値:tuple
        選択された個体二体で構成されたtupleです
        """
        total_rank = sum(xrange(len(population)))
        target = random.randrange(total_rank)
        for index, individual in enumerate(population):
            target -= (len(population)-index)
            if target < 0:
                return individual
    @classmethod
    def tournament_selection(cls, population, size,reverse = False):
        """トーナメント選択を行う関数です
        引数
        population -- 個体の集合を表すListです
        size -- トーナメントサイズを表すintです
        戻り値:tuple
        選択された個体二体で構成されたtupleです
        """
        if(not reverse):
            return max(random.sample(population, size), key=lambda x: x[1])
        else:
            return min(random.sample(population, size), key=lambda x: x[1])
    @classmethod
    def elite_selection(cls, population, size):
        """
        エリート選択を行う関数です
        population -- 固体の集合を表すListです
        size -- トーナメントサイズを示すintです
        戻り値:tuple
        選択された個体で構成されたtupleです
        """
        return population[:size]
    @classmethod
    def crossover(cls, parents, crossover_type):
        """
        親と交叉種別を受け取って交叉を行う関数です
        parents -- 個体二つを含むListです
        crossover_type -- 交叉種別を指定するintです
        args -- 選択で用いられる値を格納したdictです。
                以下の値を利用する可能性があります。
                point -- 多点交叉を行うときに交差点の個数として利用さるint型です
                genom_size -- BIT列に対して交叉を行うときに利用される
                            　BIT長を示すint型です
        戻り値:list
        交差結果として得られた個体二体で構成されるlistです
        """
        children = []
        if crossover_type == cls.CROSSOVER['CX']:
            children = cls.cx_crossover(parents)
        elif crossover_type == cls.CROSSOVER['OX']:
            children = cls.ox_crossover(parents)
        elif crossover_type == cls.CROSSOVER['PMX']:
            children = cls.pmx_crossover(parents)
        return children
    @classmethod
    def cx_crossover(cls, parents):
        """
        cycle crossoverを行う関数です
        parents -- 交叉に用いる二体の親個体です
        戻り値:list
        交差結果として得られた個体二体で構成されるlistです
        """
        size = len(parents[0][0])
        parents = [parent[0] for parent in parents]
        children = []
        children.append([None for x in xrange(size)])
        children.append([None for x in xrange(size)])
        carrent_gene = 0
        cycled = [0]
        start = 0
        children[0][0] = parents[0][0]
        children[1][0] = parents[1][0]
        while True:
            carrent_gene = parents[0].index(parents[1][carrent_gene])
            cycled.append(carrent_gene)
            children[0][carrent_gene] = parents[0][carrent_gene]
            children[1][carrent_gene] = parents[1][carrent_gene]
            if carrent_gene == start:
                if children[0].count(None) > 0:
                    children.reverse()
                    for index in xrange(size):
                        if index not in cycled:
                            carrent_gene = index
                            start = index
                            cycled.append(index)
                            break
                else:
                    break
        return ((tuple(children[0]), 0), (tuple(children[1]), 0))
    @classmethod
    def ox_crossover(cls, parents):
        """
        order crossoverを行う関数です
        parents -- 交叉に用いる二体の親個体です
        戻り値:list
        交差結果として得られた個体二体で構成されるlistです
        """
        #親の分解
        parents = [parent[0] for parent in parents]
        #順列のサイズ
        size = len(parents[0])
        #要素の選択
        select = random.sample(xrange(size), 2)
        select.sort()
        #シーケンスの作成
        sequences = []
        sequences.append(list(parents[0][select[1]:]))
        sequences[0].extend(parents[0][:select[1]])
        sequences.append(list(parents[1][select[1]:]))
        sequences[1].extend(parents[1][:select[1]])
        #選択された部分のみを持つ子を作成
        children = []
        children.append(list(parents[0][:]))
        children[0][select[1]:] = [None for i in children[0][select[1]:]]
        children[0][:select[0]] = [None for i in children[0][:select[0]]]
        children.append(list(parents[1][:]))
        children[1][select[1]:] = [None for i in children[1][select[1]:]]
        children[1][:select[0]] = [None for i in children[1][:select[0]]]
        #シーケンスより固定されている要素の削除
        #sequences[0]からchildren[1]に含まれる要素を削除
        sequences[0] = [x for x in sequences[0] if x not in children[1]]
        #sequences[1]からchildren[0]に含まれる要素を削除
        sequences[1] = [x for x in sequences[1] if x not in children[0]]
        #子にシーケンスを挿入
        children[0][select[1]:] = [sequences[1][i] for i in xrange(len(children[0][select[1]:]))]
        children[0][:select[0]] = sequences[1][len(children[0][select[1]:]):]
        children[1][select[1]:] = [sequences[0][i] for i in xrange(len(children[1][select[1]:]))]
        children[1][:select[0]] = sequences[0][len(children[1][select[1]:]):]
        return ((tuple(children[0]), 0), (tuple(children[1]), 0))
    @classmethod
    def pmx_crossover(cls, parents):
        """
        Partially-mapped crossoverを行う関数です
        parents -- 交叉に用いる二体の親個体を含むlistです
        戻り値:list
        交差結果として得られた個体二体で構成されるlistです
        """
        #親の分解
        parents = [parent[0] for parent in parents]
        #順列のサイズ
        size = len(parents[0])
        #要素の選択
        select = random.sample(xrange(size), 2)
        select.sort()
        #子の作成
        children = []
        children.append(list(parents[0][:]))
        children.append(list(parents[1][:]))
        children[0][select[0]:select[1]] = parents[1][select[0]:select[1]]
        children[1][select[0]:select[1]] = parents[0][select[0]:select[1]]
        change_map = []
        change_map.append(list(parents[0][select[0]:select[1]]))
        change_map.append(list(parents[1][select[0]:select[1]]))
        for i in xrange(len(change_map[0])):
        	if change_map[0][i] == change_map[1][i]:
        		change_map[0][i] = change_map[1][i] = None
        while None in change_map[0]:
        	change_map[0].remove(None)
        	change_map[1].remove(None)
        for s_2 in change_map[1][:]:
        	if s_2 in change_map[0]:
        		i = change_map[0].index(s_2)
        		change_map[1][change_map[1].index(s_2)] = change_map[1][i]
        		change_map[0][i] = change_map[1][i] = None
        for i in xrange(len(change_map[0])):
        	if change_map[0][i] == change_map[1][i]:
        		change_map[0][i] = change_map[1][i] = None
        while None in change_map[0]:
        	change_map[0].remove(None)
        	change_map[1].remove(None)
        range_list = [x for x in xrange(select[0], select[1])]
        cls.__pmx_list_convert(children[0], change_map[0], change_map[1], range_list)
        cls.__pmx_list_convert(children[1], change_map[0], change_map[1], range_list)
        return ((tuple(children[0]), 0), (tuple(children[1]), 0))
    @classmethod
    def __pmx_list_convert(cls, target_list, change_map1, change_map2, conved=None):
        """
        PMXに用いる関数
        """
        conved = conved[:] if conved is not None else []
        for source in change_map1:
            if source in target_list:
                i = target_list.index(source)
                if i in conved:
                    if source in target_list[i+1:]:
                        i = i + target_list[i+1:].index(source) + 1
                if i not in conved:
                    target_list[i] = change_map2[change_map1.index(source)]
                    conved.append(i)
        for source in change_map2:
            if source in target_list:
                i = target_list.index(source)
                if i in conved:
                    if source in target_list[i+1:]:
                        i = i + target_list[i+1:].index(source) + 1
                if i not in conved:
                    target_list[i] = change_map1[change_map2.index(source)]
    @classmethod
    def mutation(cls, individual, rate):
        """
        個体の突然変異を行う関数です
        individual -- 突然変異を行う個体です
        rate -- 遺伝子の変異確率を示すintです
        戻り値:individual
        突然変異を行った後の個体です
        """
        if rate == 0:
            return
        genom = list(individual[0])
        for index, gene in enumerate(genom):
            if random.randrange(rate) == 0:
                old_genom = genom[:]
                old_genom.remove(gene)
                target_index = genom.index(random.sample(old_genom, 1)[0])
                genom[index], genom[target_index] = genom[target_index], genom[index]
        return (tuple(genom), 0)
def test():
    """
    テスト用関数
    """
    test_population = [random_individual(genom_size=50, fitness=x+1) for x in xrange(100)]
    gam = GAUtility()
    args = {'tournament_size':2500}
    print(test_population)
    for selection_name, selection_type in GAUtility.SELECTION.items():
        for crossover_name, crossover_type in GAUtility.CROSSOVER.items():
            print(selection_name, crossover_name)
            ng_pop = gam.next_generation(   test_population,
                                            selection_type,
                                            crossover_type,
                                            100,
                                            **args)
            print(ng_pop)
def module_next_generation(population,selection_type,crossover_type,mutation_rate,tournament_size,elete_size,reverse):
    """
    unity から呼び出された際に使用する、集団から次世代の個体を生成する関数です
    population -- 親集団
    selection_type -- 選択の種別を示すintです
    crossover_type -- 交叉の種別を示すintです
    mutation_rate --　突然変異の確立を表すintです
                      1/mutation_rateの確立で変異します
    tournament_size　-- トーナメント選択において使用する値です
    elete_size　-- エリート選択に利用される値です
    reverse -- 最大化問題又は最小化問題のどちらであるかを指定します、
               Trueならば最小化、Falseならば最大化です
    """
    args = {'tournament_size':tournament_size,'elete_size':elete_size,'reverse':reverse}
    ng_pop = GAUtility.next_generation( population,
                                        GAUtility.SELECTION[selection_type],
                                        GAUtility.CROSSOVER[crossover_type],
                                        mutation_rate,
                                        **args)
    global ng_genoms
    ng_genoms = tuple([individual[0] for individual in ng_pop])
def random_individual(genom_size=10, fitness=0):
    """
    ランダムな個体を作成し返す関数です
    genom_size -- 遺伝子の長さを示すintです
    fitness -- 適応度を示すintです
    """
    genom = range(genom_size)
    random.shuffle(genom)
    return (tuple(genom), fitness)
if __name__ == '__main__':
    test()
if __name__ == '<module>':
    populatinon = tuple(zip(genoms,fitnesses))
    module_next_generation( populatinon,
                            selection,
                            crossover,
                            mutation_rate,
                            tournament_size,
                            elete_size,
                            reverse)
