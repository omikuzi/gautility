#include <Python.h>
#include <stdlib.h>
#include <time.h>
int isInclude(int*,int,int);
static PyObject *
int2bit(PyObject *self,PyObject *args){
	int num,len,w,i;
	int* result_int;
	PyObject* result;
	if(!PyArg_ParseTuple(args, "ii", &num,&len)){
		PyErr_SetString(PyExc_ValueError,"Invalid argument");
		return NULL;
	}
	w = 1 << (len-1);
	if(num > w-1 || num < -w){
		PyErr_SetString(PyExc_ValueError,"Invalid argument");
		return NULL;
	}
	result_int = (int*)calloc(len,(sizeof(int)));
	w = 1 << (len - 1);
	for(i = len-1;i >= 0;i--){
		result_int[i] = (num & w)?1:0;
		num <<= 1;
	}
	result = PyList_New(0);
	for(i = 0;i<len;i++){
		PyList_Append(result,PyLong_FromLong((long)result_int[i]));
	}
	free(result_int);
	return result;
}


static PyObject *
multipoint_crossover(PyObject *self,PyObject *args){
	int point,size,w,p,j,i;
	int results[2];
	int genes[2];
	int* points;
	for(i = 0;i<2;i++){
		results[i] = 0;
	}
	if(!PyArg_ParseTuple(args, "iiii",&point,genes,genes+1,&size)){
		PyErr_SetString(PyExc_ValueError,"Invalid argument");
		return NULL;
	}
	if(point > size){
		PyErr_SetString(PyExc_ValueError,"Invalid argument");
		return NULL;
	}
	srand((unsigned)time(NULL));
	points = (int*)calloc(size,(sizeof(int)));
	for(i = 0;i<point;i++){
		w = rand()%size;
		while(isInclude(points,i,w)){
			w = rand()%size;
		}
		points[i] = w;
	}
	w = p = 0;
	j = 1;
	for(i = 0;i<size;i++){
		if(p < point && points[p] == i){
			w = w^1;
			p++;
		}
		results[0] += genes[w]&j;
		results[1] += genes[w^1]&j;
		j <<= 1;
	}
	for(i = 0;i<2;i++){
		if(results[i] & (j>>1)){
			results[i] = -j+results[i];
		}
	}
	free(points);
	return Py_BuildValue("ii",results[0],results[1]);
}

static PyObject *
uniform_crossover(PyObject *self,PyObject *args){
	int size,i,j,w;
	int genes[2];
	int results[2];
	for(i = 0;i<2;i++){
		results[i] = 0;
	}
	if(!PyArg_ParseTuple(args, "iii",genes,genes+1,&size)){
		PyErr_SetString(PyExc_ValueError,"Invalid argument");
		return NULL;
	}
	srand((unsigned)time(NULL));
	for(i = 0,j = 1;i<size;i++,j <<= 1){
		w = rand()%2;
		results[0] += genes[w]&j;
		results[1] += genes[w^1]&j;
	}
	for(i = 0;i<2;i++){
		if(results[i] & (j>>1)){
			results[i] = -j+results[i];
		}
	}
	return Py_BuildValue("ii",results[0],results[1]);
}

static int isInclude(int* a,int size,int t){
	int i;
	for(i=0;i<size;i++){
		if(a[i] == t)return 1;
	}
	return 0;
}

static PyObject *
gene_mutation(PyObject *self,PyObject *args){
	int gene,size,rate,i,j,result = 0;
	if(!PyArg_ParseTuple(args, "iii",&gene,&size,&rate)){
		PyErr_SetString(PyExc_ValueError,"Invalid argument");
		return NULL;
	}
	srand((unsigned)time(NULL));
	for(i = 0,j = 1;i<size;i++,j<<=1){
		if(rand()%rate){
			result += gene&j;
		}else{
			result += (gene&j)?0:j;
		}
	}
	if(result & (j>>1)){
		result = -j+result;
	}
	return PyLong_FromLong((long)result);
}

static PyMethodDef individual_cross_methods[] = {
	{"int2bit",int2bit,METH_VARARGS,"int convert to bit array"},
	{"multipoint_crossover",multipoint_crossover,METH_VARARGS,"individual genes multipoint cross over"},
	{"uniform_crossover",uniform_crossover,METH_VARARGS,"individual genes uniform crossover"},
	{"gene_mutation",gene_mutation,METH_VARARGS,"individual gene mutation"},
	{NULL,NULL,0,NULL}
};

static struct PyModuleDef individual_cross_module = {
	PyModuleDef_HEAD_INIT,
	"individual_cross",
	"none",
	-1,
	individual_cross_methods
};

PyMODINIT_FUNC
PyInit_individual_cross(void){
	return PyModule_Create(&individual_cross_module);
}
